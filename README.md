# Telegraf SSM docker image

Custom Telegraf docker image for SSM purposes (CERN CentOS + Oracle drivers ...)

# How to release

First, start a gitflow release branch and update the version to a non-SNAPSHOT:

    export NEW_VERSION=<new version>
    
    git flow release start $NEW_VERSION
    mvn versions:set -DnewVersion=$NEW_VERSION
    git commit -a -m "Preparing version $NEW_VERSION"
    
Then, refine the release as needed. When you are ready :

    git flow release finish $NEW_VERSION
    git push --tags origin
    
The release will be automatically deployed by Gitlab CI.

Once back on the develop branch, update the version and git push

    mvn versions:set -DnewVersion=<new SNAPSHOT version>
    git commit -a -m "Preparing next SNAPSHOT" && git push