FROM gitlab-registry.cern.ch/linuxsupport/c8-base


ENV LANG C.UTF-8

ADD influxdb.repo /etc/yum.repos.d/influxdb.repo

ADD entrypoint.sh /

ADD slot3 /usr/local/bin/slot3

ADD telegraf.conf /etc/telegraf/telegraf.conf

RUN yum update -y && yum install -y telegraf libnsl python3 python3-devel gcc gcc-c++ unixODBC-devel ipmitool && \
    yum clean all

RUN curl -o /tmp/orainst-basic.rpm https://linuxsoft.cern.ch/mirror/yum.oracle.com/repo/OracleLinux/OL8/oracle/instantclient/x86_64/getPackage/oracle-instantclient19.9-basic-19.9.0.0.0-1.x86_64.rpm && \
    curl -o /tmp/orainst-sqlplus.rpm https://linuxsoft.cern.ch/mirror/yum.oracle.com/repo/OracleLinux/OL8/oracle/instantclient/x86_64/getPackage/oracle-instantclient19.9-sqlplus-19.9.0.0.0-1.x86_64.rpm && \
    curl https://packages.microsoft.com/config/rhel/8/prod.repo > /etc/yum.repos.d/mssql-release.repo && \
    yum localinstall -y /tmp/orainst-basic.rpm && \
    yum localinstall -y /tmp/orainst-sqlplus.rpm && \
    yum install oracle-release.noarch -y && \
    yum install oracle-instantclient-tnsnames.ora.noarch -y && \
    ACCEPT_EULA=Y yum install msodbcsql17 -y && \
    pip3 install influxdb pyodbc && \
    yum install unixODBC -y && \
    rm /tmp/orainst*.rpm 

ENTRYPOINT ["/entrypoint.sh"]
CMD ["telegraf"]
